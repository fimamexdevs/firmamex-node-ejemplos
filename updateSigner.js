const FirmamexServices = require('../node/firmamex');

const webId = ""
const apiKey = ""

const services = new FirmamexServices(webId, apiKey);

const ticket = ''
const sticker_id = ''

async function updateSigner() {

    try {

    const responseSticker = await services.updateSigner(ticket, sticker_id, {
        authority: 'Vinculada a Correo Electronico por Liga',
        email: 'email@firmamex.com',
        data: 'email@firmamex.com',
        imageType: 'desc',
        // updateAll: true
    })

    console.log({responseSticker})

    } catch(e) {
        console.error({
            status: e.response.status,
            message: e.response.statusText,
            data: e.response.data
        })
    }
}


updateSigner()