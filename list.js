
const FirmamexServices = require('../node/firmamex');

const webId = ""
const apiKey = ""

const services = FirmamexServices(webId, apiKey);


async function list() {

    const from = new Date('2021-12-01');
    const to = new Date('2022-01-12')

    const docs = new Set();
    
    let moreResults = true;
    let nextToken = null;

    while(moreResults) {


        const result = await services.listDocuments(from.getTime(), to.getTime(), nextToken);

        console.log(`
            Got ${result.documents.length} documents
            and token ${result.nextToken}
        `)
        
        for(let doc of result.documents) {
            docs.add(doc.firmamexId)
        }        

        nextToken = result.nextToken;

        moreResults = nextToken != null;

    }

    console.log(docs.size)
}

list();