/**
 * Ejemplo para obtener una estampilla de tiempo
 */


const crypto = require('crypto')

const FirmamexServices = require('../node/firmamex');

const webId = "LJqCZKPII3FfLxPS"
const apiKey = "ddc47394e98f750ea42fd61026526f21"

const services = FirmamexServices(webId, apiKey);

async function timestamp() {

    const hash = crypto.createHash('sha256')
    hash.update('datos a estampillar');

    const response = await services.timestamp({
        hash: hash.digest('hex')
    })
    console.log(response);
}

timestamp();