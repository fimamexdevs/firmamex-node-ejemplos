/**
 * Ejemplo para generar un documento a partir de una URL colocando
 * los stickers por medio del API
 */

const FirmamexServices = require('../node/firmamex');

const webId = "LJqCZKPII3FfLxPS"
const apiKey = "ddc47394e98f750ea42fd61026526f21"

const services = FirmamexServices(webId, apiKey);

async function requestSticker() {
    const response = await services.request({ 
        "url_doc" : "https://www.dropbox.com/s/sxvgq1uhb4k3s4w/contrato.pdf?dl=0",
        "view_mode":"simple", 
        "stickers": [{
            "authority":"SAT",
            "stickerType":"line",
            "dataType":"rfc",
            "imageType":"desc",
            "data":"GOCF9002226A711",
            "page":0,
            "rect": {
                "lx":355,
                "ly":102,
                "tx":555,
                "ty": 202
            }
        }]
    })
    console.log(response);
}

requestSticker();