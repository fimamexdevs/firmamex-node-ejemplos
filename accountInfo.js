
const FirmamexServices = require('../firmamex-node/firmamex');


const webId = ""
const apiKey = ""

const services = new FirmamexServices(webId, apiKey);

async function getAccountInfo() {

    try {
        const response = await services.getAccountInfo();
        console.log(response)
    } catch (e) {
        console.error({
            status: e.response.status,
            message: e.response.statusText,
            data: e.response.data
        })
    }

}

getAccountInfo()