
const fs = require('fs');

 const FirmamexServices = require('../node/firmamex');
 
 const webId = ""
 const apiKey = ""
 
 const services = FirmamexServices(webId, apiKey);

async function getDocumentForStamp() {

    const frmxId = '';

    const {timestamps} = await services.getNom151StampsForDocument(frmxId);
    const stamp = timestamps[0];
    
    fs.writeFileSync('stamp.tsr', Buffer.from(stamp.timestamp, 'base64'));

    const document = services.getDocumentForStamp(frmxId, stamp.hash);
    
    fs.writeFileSync('doc.pdf', Buffer.from(document.data, 'base64'));

}

getDocumentForStamp();