/**
 * Ejemplo de inyectar datos en un template HTML 
 * con dos stickers
 */


const FirmamexServices = require('../node/firmamex');

const webId = "LJqCZKPII3FfLxPS"
const apiKey = "ddc47394e98f750ea42fd61026526f21"

const services = FirmamexServices(webId, apiKey);

async function fillTemplate() {
    const response = await services.request({
        template_title: 'ejemplo1',
        fields: [
            {"id":"codigo","value":"1231210"},
            {"id":"proveedor","value":"ÁÉÍ"},
            {"id":"empresa","value":""},
            {"id":"sign_1","value":"Firmamex GOCF9002226A7 line stroke"},
            {"id":"sign_2","value":"Firmamex fernando@firmamex.com"}
        ],
        tables: [{
            id: 'directory',
            rows: [
                ['clave', 'desc', 'cant', 'precio', 'total'],
                ['clave 2', 'desc 2', 'cant 2', 'precio 2', 'total 2'],
            ]
        }]
    })
    console.log(response);
}

fillTemplate();